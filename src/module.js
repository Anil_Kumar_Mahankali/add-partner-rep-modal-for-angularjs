import angular from 'angular';
import AddPartnerRepModal from './addPartnerRepModal';
import repEmailAddressValidator from './repEmailAddress.validator';
import errorMessagesTemplate from './error-messages.html!text';
import 'angular-bootstrap';
import 'angular-messages';
import 'bootstrap/css/bootstrap.css!css'
import 'bootstrap'

angular
    .module(
        'addPartnerRepModal.module',
        [
            'ui.bootstrap',
            'ngMessages'
        ]
    )
    .directive(
        'repEmailAddressValidator',
        [
            '$q',
            'partnerRepServiceSdk',
            'sessionManager',
            repEmailAddressValidator
        ]
    )
    .service(
        'addPartnerRepModal',
        [
            '$modal',
            AddPartnerRepModal
        ]
    )
    .run(
        [
            '$templateCache',
            $templateCache => {
                $templateCache.put(
                    'add-partner-rep-modal/error-messages.html',
                    errorMessagesTemplate
                );
            }
        ]
    );