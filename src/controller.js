import PartnerRepServiceSdk,{AddPartnerRepReq} from 'partner-rep-service-sdk';
import SessionManager from 'session-manager';

export default class Controller {

    _$modalInstance;

    _partnerRepServiceSdk:PartnerRepServiceSdk;

    _addPartnerRepReqData = {};

    _sessionManager:SessionManager;

    _$scope;

    constructor($modalInstance,
                partnerRepServiceSdk:PartnerRepServiceSdk,
                sessionManager:SessionManager,
                $scope
    ) {

        if (!$modalInstance) {
            throw new TypeError('$modalInstance required');
        }
        this._$modalInstance = $modalInstance;

        if (!partnerRepServiceSdk) {
            throw new TypeError('partnerRepServiceSdk required');
        }
        this._partnerRepServiceSdk = partnerRepServiceSdk;

        if (!sessionManager) {
            throw new TypeError('sessionManager required');
        }
        this._sessionManager = sessionManager;

        this._$scope = $scope;

    }

    get addPartnerRepReqData() {
        return this._addPartnerRepReqData;
    }

    cancel():void {
        this._$modalInstance.dismiss();
    }

    saveAndClose(form):void {
        if (form.$valid && !form.$pending) {
            this._$scope.loader = true;
            Promise
                .all(
                    [
                        this._sessionManager
                            .getUserInfo(),
                        this._sessionManager
                            .getAccessToken()
                    ]
                )
                .then(results => this._partnerRepServiceSdk
                    .addPartnerRep(
                        new AddPartnerRepReq(
                            this._addPartnerRepReqData.firstName,
                            this._addPartnerRepReqData.lastName,
                            this._addPartnerRepReqData.emailAddress,
                            results[0].account_id),
                        results[1]
                    )
                )
                .then(partnerRepId => {
                    this._$scope.loader = false;
                    this._$modalInstance.close(partnerRepId)
                    }
                );
        }
        else {
            form.$setSubmitted();
        }
    }
}

Controller.$inject = [
    '$modalInstance',
    'partnerRepServiceSdk',
    'sessionManager',
    '$scope'
];

