import addPartnerRepTemplate from './add-partner-rep.html!text';
import AddPartnerRepController from './addPartnerRepController';

export default class RouteConfig {

    constructor($routeProvider) {

        $routeProvider
            .when
            (
                '/',
                {
                    template: addPartnerRepTemplate,
                    controller: AddPartnerRepController,
                    controllerAs: 'controller'
                }
            );

    }

}

RouteConfig.$inject = [
    '$routeProvider'
];